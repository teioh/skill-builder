Imported from my github

This web app is based on skill curve theory and allows a user to guide themselves to acquiring a new ability. After login, users are asked to define a skill they'd like to learn, then break it down into five steps. They can log in every day and log what they've done in their 45 minutes of practice. It's all based in actual science. 

This application was designed by me, Joe Frew, and created in partnership with Michael Megarbane. We learned a lot from the project, especially when it came to React and D3. 

Build time was ~15 hours.

Things to add: 
#Text message reminder system
#Better sanitation passing text to the db
#More expansive explanation of the science behind the application
#Probably some citations, too